import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ShoppingCartTest {
ShoppingCart cart;
	@Before
	public void setUp() throws Exception
	{
		// 1. Making a shopping cart
	 cart = new ShoppingCart();
	}
	@Test
	public void testAddProductToCart() {
		//E1: When a new product is added, the number of items must be incremented  
		//E2: When a new product is added, the new balance must be the sum of the previous balance plus the cost of the new product  
// iteration one of pour test case
 
 
		
  //2. make a product
		Product phone = new Product("iphone", 1500);
 // check bal in cart before adding prod
			double startBalance = cart.getBalance();
			assertEquals(0, startBalance,0.01);
 //* 4. check the number of item in the cart - prev num items
			int startingNumItems = cart.getItemCount();
			assertEquals(0, startingNumItems);
 //* 5.Add prod to the cart
			cart.addItem(phone);
 //* 6. check if updated or not(previous balance + price of added items)
			assertEquals(startingNumItems + 1, cart.getItemCount());
			assertEquals(1, cart.getItemCount());
			
 //* 
 //*/
			// check the updated balance in the cart
			// prev bal + price
			double expectedBalance = startBalance + phone.getPrice();
			assertEquals(expectedBalance, cart.getBalance(), 0.01);
	}

}
